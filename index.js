// Dom elementi
const gameGrid = document.querySelector('#game');
const scoreTable = document.querySelector('#score');
const startButton = document.querySelector('#start-button');
// Game constanti
const POWER_PILL_TIME = 10000; // ms
const GLOBAL_SPEED = 40; // ms
const gameBoard = GameBoard.createGameBoard(gameGrid, LEVEL);
// purvichna nastroika
let score = 0;
let timer = null;
let gameWin = false;
let powerPillActive = false;
let powerPillTimer = null;

// audio
function playAudio(audio) {
  const soundEffect = new Audio(audio);
  soundEffect.play();
}

// game controler
function gameOver(pacman, grid) {


  document.removeEventListener('keydown', (e) =>
    pacman.handleKeyInput(e, gameBoard.objectExist.bind(gameBoard))
  );

  gameBoard.showGameStatus(gameWin);

  clearInterval(timer);
  // pokazva startbutton
  startButton.classList.remove('hide');
}

function checkCollision(pacman, ghosts) {
  const collidedGhost = ghosts.find((ghost) => pacman.pos === ghost.pos);

  if (collidedGhost) {
    if (pacman.powerPill) {

      gameBoard.removeObject(collidedGhost.pos, [
        OBJECT_TYPE.GHOST,
        OBJECT_TYPE.SCARED,
        collidedGhost.name
      ]);
      collidedGhost.pos = collidedGhost.startPos;
      score += 100;
    } else {
      gameBoard.removeObject(pacman.pos, [OBJECT_TYPE.PACMAN]);
      gameBoard.rotateDiv(pacman.pos, 0);
      gameOver(pacman, gameGrid);
    }
  }
}

function gameLoop(pacman, ghosts) {
  // 1. murda pacman
  gameBoard.moveCharacter(pacman);
 // 2. proverqva colissions na duhovete na starite pozicii
  checkCollision(pacman, ghosts);
  // 3. murda duhovete
  ghosts.forEach((ghost) => gameBoard.moveCharacter(ghost));
  // 4. nov collision na duhovete
  checkCollision(pacman, ghosts);
  // 5. proverqva dali pacman qde tochki
  if (gameBoard.objectExist(pacman.pos, OBJECT_TYPE.DOT)) {

    gameBoard.removeObject(pacman.pos, [OBJECT_TYPE.DOT]);
    // premahva izqdenite tochki
    gameBoard.dotCount--;
    // dobavq resultat
    score += 10;
  }
  // 6. proverqva dali pacman e izql powerpill
  if (gameBoard.objectExist(pacman.pos, OBJECT_TYPE.PILL)) {

    gameBoard.removeObject(pacman.pos, [OBJECT_TYPE.PILL]);

    pacman.powerPill = true;
    score += 50;

    clearTimeout(powerPillTimer);
    powerPillTimer = setTimeout(
      () => (pacman.powerPill = false),
      POWER_PILL_TIME
    );
  }
  // 7. scare mode na duhovete ako e izqden powerpill
  if (pacman.powerPill !== powerPillActive) {
    powerPillActive = pacman.powerPill;
    ghosts.forEach((ghost) => (ghost.isScared = pacman.powerPill));
  }
  // 8. proverqva dali vsichki tochki sa izqdeni
  if (gameBoard.dotCount === 0) {
    gameWin = true;
    gameOver(pacman, gameGrid);
  }
  // 9. pokazva noviq resultat
  scoreTable.innerHTML = score;
}

function startGame() {


  gameWin = false;
  powerPillActive = false;
  score = 0;

  startButton.classList.add('hide');

  gameBoard.createGrid(LEVEL);

  const pacman = new Pacman(2, 287);
  gameBoard.addObject(287, [OBJECT_TYPE.PACMAN]);
  document.addEventListener('keydown', (e) =>
    pacman.handleKeyInput(e, gameBoard.objectExist.bind(gameBoard))
  );

  const ghosts = [
    new Ghost(5, 188, randomMovement, OBJECT_TYPE.BLINKY),
    new Ghost(4, 209, randomMovement, OBJECT_TYPE.PINKY),
    new Ghost(3, 230, randomMovement, OBJECT_TYPE.INKY),
    new Ghost(2, 251, randomMovement, OBJECT_TYPE.CLYDE)
  ];

  // Gameloop
  timer = setInterval(() => gameLoop(pacman, ghosts), GLOBAL_SPEED);
}

// inicializira igrata
startButton.addEventListener('click', startGame);